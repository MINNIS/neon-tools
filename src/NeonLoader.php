<?php

namespace MINNIS\NeonTools;

class NeonLoader
{
    const CACHE_SUFFIX = '.cache';

    /** @var bool */
    private $cacheEnabled;

    /** @var array|null */
    private $selectKeys;

    /** @var array|null */
    private $excludeKeys;

    /** @var array<string,int> */
    private $importIndex;

    /**
     * @param bool $cacheEnabled (default true)
     * @param array|string|null $selectKeys
     * @param array|string|null $excludeKeys
     */
    public function __construct($cacheEnabled = true, $selectKeys = null, $excludeKeys = null)
    {
        $this->cacheEnabled = (bool)$cacheEnabled;
        $this->selectKeys   = is_string($selectKeys) ? array($selectKeys) : $selectKeys;
        $this->excludeKeys  = is_string($excludeKeys) ? array($excludeKeys) : $excludeKeys;
        $this->importIndex  = array();
    }

    /**
     * @param string $file
     * @param string|array|null $selectKeys
     * @param string|array|null $excludeKeys
     * @return array
     * @throws \Exception
     */
    public static function parseFile($file, $selectKeys = null, $excludeKeys = null)
    {
        $fileloaderInstance = new self(true, $selectKeys, $excludeKeys);

        return $fileloaderInstance->fromFile($file);
    }

    /**
     * @param string $file
     * @param string|array|null $selectKeys
     * @param string|array|null $excludeKeys
     * @return array
     * @throws \Exception
     */
    public static function parseFileWithoutCache($file, $selectKeys = null, $excludeKeys = null)
    {
        $fileloaderInstance = new self(false, $selectKeys, $excludeKeys);

        return $fileloaderInstance->fromFile($file);
    }

    /**
     * @param string $file
     * @return array
     * @throws \Exception
     */
    public function fromFile($file)
    {
        if ($this->cacheEnabled === true && $this->cacheExists($file)) {

            $cache = $this->loadCache($file);

            if (false !== $cache) {

                if (null === $this->selectKeys && null === $this->excludeKeys) {

                    return $cache;
                }

                return $this->fromCacheUsingFilters($cache);
            }
        }

        if (!file_exists($file)) {
            throw new \Exception('File ' . basename($file) . ' not found');
        }

        if (null === $this->selectKeys && null === $this->excludeKeys) {

            return $this->loadReal($file);
        }

        return $this->fromFileUsingFilters($file);
    }

    /**
     * @param string $file
     * @return array
     */
    private function fromFileUsingFilters($file)
    {
        $neonArray = $this->fromFileUsingSelect($file);

        if (empty($this->excludeKeys)) {
            return $neonArray;
        }

        foreach ($this->excludeKeys as $excludeKey) {

            unset($neonArray[$excludeKey]);
        }

        return $neonArray;
    }

    /**
     * @param string $file
     * @return array
     */
    private function loadReal($file)
    {
        $neon = \Nette\Neon\Neon::decode(file_get_contents($file));

        $this->importIndex[realpath($file)] = filemtime($file);

        if (!empty($neon['imports'])) {

            $neon = array_merge_recursive($neon, $this->procesImports($this->pathConversion($file, $neon['imports'])));

            unset($neon['imports']);
        }

        if (!empty($neon['parameters'])) {

            $this->parseParameters($neon, $neon['parameters']);
        }

        if (false === $this->cacheEnabled) {
            return $neon;
        }

        $neon['__imports__'] = $this->importIndex;

        file_put_contents($file . self::CACHE_SUFFIX, serialize($neon));

        unset($neon['__imports__']);

        return $neon;
    }

    /**
     * @param array &$neon (by reference)
     * @param array $parameters
     */
    private function parseParameters(&$neon, $parameters)
    {
        array_walk_recursive($neon, function (&$value, $key) use ($parameters) {

            if (is_string($value) && strpos($value, '%') == 0 && substr($value, -1) === '%') {

                $placeholder = str_replace('%', '', $value);

                if (array_key_exists($placeholder, $parameters)) {

                    $value = $parameters[$placeholder];
                }
            }
        });
    }

    /**
     * @param string $parentfile
     * @param array $imports
     * @return array
     * @throws \Exception
     */
    private function pathConversion($parentfile, $imports)
    {
        $starting_path = realpath(dirname($parentfile));

        $stamp = 0;

        foreach ($imports as &$import) {

            if (is_array($import) && key($import) === 'resource') {
                // yaml support
                $import = (string)current($import);
            }

            if (strpos($import, DIRECTORY_SEPARATOR) !== 0) {
                $import = $starting_path . DIRECTORY_SEPARATOR . $import;
            }

            if (!file_exists($import)) {
                throw new \Exception(basename($parentfile) . ' is trying to include ' . basename($import) . ' but it seems absent');
            }

            if (array_key_exists($import, $this->importIndex)) {
                // do not include the same file more than once
                continue;
            }

            if ($this->cacheEnabled) {
                $stamp = filemtime($import);
            }

            $this->importIndex[$import] = $stamp;
        }

        return $imports;
    }

    /**
     * @param array $imports
     * @return array
     * @throws \Exception
     */
    private function procesImports($imports)
    {
        foreach ($imports as $import) {

            $neon = \Nette\Neon\Neon::decode(file_get_contents($import));

            if (!empty($neon['imports'])) {

                $neon = array_merge_recursive($neon, $this->procesImports($this->pathConversion($import, $neon['imports'])));
            }
        }

        return $neon;
    }

    /**
     * @param string $file
     * @return array
     */
    private function fromFileUsingSelect($file)
    {
        $rawNeonArray = $this->loadReal($file);

        if (empty($this->selectKeys)) {

            return $rawNeonArray;
        }

        $neonArray = array();

        foreach ($this->selectKeys as $selectKey) {

            if (array_key_exists($selectKey, $rawNeonArray)) {

                $neonArray[$selectKey] = $rawNeonArray[$selectKey];
            }
        }

        return $neonArray;
    }

    /**
     * @param string $file
     * @return bool
     */
    private function cacheExists($file)
    {
        if (!file_exists($file . self::CACHE_SUFFIX)) {
            return false;
        }

        return true;
    }

    /**
     * @param string $file
     * @return array|false
     */
    public function loadCache($file)
    {
        if (PHP_MAJOR_VERSION === 5) {
            $cached = unserialize(file_get_contents($file . self::CACHE_SUFFIX));
        } else {
            $cached = unserialize(file_get_contents($file . self::CACHE_SUFFIX), array('DateTime', 'DateTimeImmutable', 'Entity'));
        }

        foreach ($cached['__imports__'] as $importFile => $stamp) {

            if ($stamp !== filemtime($importFile)) {

                return false;
            }
        }

        unset ($cached['__imports__']);

        return $cached;
    }

    /**
     * @param array $cache
     * @return array
     */
    private function fromCacheUsingFilters($cache)
    {
        $neonArray = $this->fromCacheUsingSelect($cache);

        if (empty($this->excludeKeys)) {
            return $neonArray;
        }

        foreach ($this->excludeKeys as $excludeKey) {

            unset($neonArray[$excludeKey]);
        }

        return $neonArray;
    }

    /**
     * @param array $rawNeonArray
     * @return array
     */
    private function fromCacheUsingSelect($rawNeonArray)
    {
        if (empty($this->selectKeys)) {

            return $rawNeonArray;
        }

        $neonArray = array();

        foreach ($this->selectKeys as $selectKey) {

            if (array_key_exists($selectKey, $rawNeonArray)) {

                $neonArray[$selectKey] = $rawNeonArray[$selectKey];
            }
        }

        return $neonArray;
    }
}
