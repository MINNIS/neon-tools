<?php

// Use Composer's autoloader
require_once __DIR__ . '/../../../autoload.php';

// How many iterations we would like to run
$runs = 1000;

// Test 1: No cache
$start = microtime(true);

for ($i = 1; $i <= $runs; $i++) {

    $test = MINNIS\NeonTools\NeonLoader::parseFileWithoutCache(__DIR__ . '/test.neon');
    unset($test);
}

echo round(microtime(true) - $start, 3) . ' sec' . PHP_EOL;


// Test 2: Using cache
$start = microtime(true);

for ($i = 1; $i <= $runs; $i++) {

    $test = MINNIS\NeonTools\NeonLoader::parseFile(__DIR__ . '/test.neon');
    unset($test);
}

echo round(microtime(true) - $start, 3) . ' sec' . PHP_EOL;
